BX.ready(function() {
  $(".input100").on("blur", function() {
    if ($(this).val().trim() != "") {
      $(this).addClass("has-val");
    } else {
      $(this).removeClass("has-val");
    }
  });
});
