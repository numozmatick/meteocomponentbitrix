<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/**
 * @var string $templateFolder
 *  @var array $arParams */
/** @var array $arResult */
/*echo "<pre>";print_r($arResult);echo "</pre>";*/
?>
  <div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100 p-b-160 p-t-50">
        <form class="login100-form validate-form">
          <?if(empty($arResult['RESPONSE_METEO']['nameCity'])):?>
					<span class="login100-form-title p-b-43 mb-5">
            Погода в вашем городе
					</span>
          <?else:?>
            <span class="login100-form-title p-b-43 mb-5">
            Погода в городе <?echo $arResult['RESPONSE_METEO']['nameCity']?>
					</span>
          <?endif;?>
          <div class="wrap-input100 rs1 validate-input" data-validate="Username is required">
            <input class="input100" type="text" name="CITY_NAME" value="">
            <span class="label-input100">Введите город</span>
          </div>
          <input hidden type="text" name="action" value="Met">
          <input hidden type="text" name="lang" value="ru">
          <div class="container-login100-form-btn">
            <button type="submit" class="login100-form-btn">
              Узнать погоду
            </button>
          </div>
        </form>
          <?if(!empty($arResult['RESPONSE_METEO'])):?>
            <div class="weather-table wrap-login100 p-b-160 p-t-50 bg-white mt-4">
              <h1 class="weather-table__title"><?=$arResult['RESPONSE_METEO']['nameCity'].' '.(round($arResult['RESPONSE_METEO']['main']['temp'])-273)?>&deg;</h1>
              <img class="weather-table__img" src="http://openweathermap.org/img/w/<?=$arResult['RESPONSE_METEO']["weather"][0]["icon"]?>.png" alt="погода">
              <div class="weather-table__date">
                <div class="weather-table__time"><?=date(' H:i', $arResult['RESPONSE_METEO']["dt"])?></div>

                <div><?=date('m/d/Y', $arResult['RESPONSE_METEO']["dt"])?></div>
              </div>
              <div class="weather-table__temp">
                от <?=(round($arResult['RESPONSE_METEO']['main']['temp_min'])-273)?>&deg;до <?=(round($arResult['RESPONSE_METEO']['main']['temp_max'])-273)?>&deg;
              </div>
              <div class="weather-table__sun">
                <div class="weather-table__sunrice"><span>Восход:</span><?=date('H:i', $arResult['RESPONSE_METEO']["sys"]["sunrise"])?></div>
                <div class="weather-table__sunset"><span>Закат:</span><?=date('H:i', $arResult['RESPONSE_METEO']["sys"]["sunset"])?></div>
              </div>
              <div class="weather-table__wind">Ветер:<?=$arResult['RESPONSE_METEO']["wind"]["speed"]?>м/с</div>
            </div>
          <?endif;?>
      </div>
    </div>



  </div>

<script>
  /*setInterval(() => location.reload(), 60000);*/


  /*function sendCity() {
    let apiKey = "b9a82caab604c5d354482f823fef41bb";
    let cityName = $("[name='city']").val();
    if (cityName == undefined) {
      console.log("error");
    } else {
      $.ajax({
        type: "GET",
        url: "http://api.openweathermap.org/data/2.5/weather?q=" + cityName + "&appid=" + apiKey,
        success: function(data) {
          sendMeteoData(data);
        }
      });
    }
  }

  function sendMeteoData(response) {
    $.ajax({
      type: "POST",
      url: "local/ajax/meteo.component.php",
      data: {
        response
      },
      success: function(data) {
        console.log('ok')
      }
    });
  }*/


</script>