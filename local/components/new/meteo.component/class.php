<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader,
    Bitrix\Main,
    Bitrix\Sale,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\SystemException,
    Bitrix\Main\Grid\Declension,
    Bitrix\Sale\Order,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale\Delivery,
    Bitrix\Iblock,
    Bitrix\Main\Context;

class PersonalAPI extends CBitrixComponent
{
    protected function MetAction()
    {

            $cityName = $_REQUEST["CITY_NAME"];
            $apiKey = "b9a82caab604c5d354482f823fef41bb";
            $ch = curl_init("http://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=$apiKey");
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_GET, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            $decode_response =  json_decode($response);
            foreach ($decode_response as $key=>$value) {
                $response_modifier[$key] = $this->arrayCastRecursive($value);
            }
            if ($decode_response->cod!==200){
                die();
            }
            else{
                $this->arResult['RESPONSE_METEO'] = $response_modifier;
                $this->arResult['RESPONSE_METEO']['nameCity'] = $cityName;
            }

    }

    protected function arrayCastRecursive($array)
    {
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    $array[$key] = $value;
                }
                if ($value instanceof stdClass) {
                    $array[$key] = (array)$value;
                }
            }
        }
        if ($array instanceof stdClass) {
            return (array)$array;
        }
        return $array;
    }

    protected function prepareAction()
    {

        $action = $this->request->offsetExists($this->arParams['ACTION_VARIABLE'])
            ? $this->request->get($this->arParams['ACTION_VARIABLE'])
            : $this->request->get('action');

        if (empty($action))
            $action = false;

        return $action;
    }

    protected function doAction($action)
    {
        if (is_callable(array($this, $action . "Action"))) {
            call_user_func(
                array($this, $action . "Action")
            );
        }
    }

    public function showAjaxAnswer($result)
    {

        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        die;
    }

    protected function checkRequiredModules()
    {

        if (!Loader::includeModule("sale"))
            return;

        if (!Loader::includeModule("catalog"))
            return;

        $this->registry = Sale\Registry::getInstance(Sale\Order::getRegistryType());
    }

    public function executeComponent()
    {

        $this->checkRequiredModules();
        $this->action = $this->prepareAction();

        if ($this->action)
            $this->doAction($this->action);

        if ($this->startResultCache())
            $this->includeComponentTemplate();

        return $this->arResult;
    }

}
